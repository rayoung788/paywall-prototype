import axios from 'axios'

const host = 'http://ec2-18-220-209-189.us-east-2.compute.amazonaws.com'
const api = 5559
const gateway = 5558

const downloadLink = hash => {
  return `${host}:${gateway}/ipfs/${hash}`
}

const uploadFile = file => {
  let formData = new FormData()
  formData.append('file', file)
  return axios.post(`${host}:${api}/api/v0/add`, formData, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

export { downloadLink, uploadFile }
