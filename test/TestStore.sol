pragma solidity ^0.4.11;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Store.sol";

contract TestStore {
  Store store = Store(DeployedAddresses.Store());
  // Testing the newItem() function
    // function testUserCanViewItems() {

    //     bytes32[] storage expected;
    //     bytes32[] storage returned;
    //     returned = store.viewList();

    //     Assert.equal(returned, expected, "Start list should be empty.");
    // }
  // Testing the newItem() function
    function testUserCanCreateANewItem() {
        bool returnedId = store.newItem(8, 'test', 'my_hash');

        bool expected = true;
        bytes32 list = store.viewList()[0];

        Assert.equal(returnedId, expected, "New item should be created.");
    }

}