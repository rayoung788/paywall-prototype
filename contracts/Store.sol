pragma solidity ^0.4.4;
contract Store {

    struct Item {
        uint price;     // price of the item
        string title;   // title of the item
        address seller; // entity selling the item
        string hashID;   // hash of the item
        uint listPointer;
    }

    mapping(bytes32 => Item) activeItems;
    mapping(bytes32 => string) purchased;
    bytes32[] public itemList;

    function isOpen(bytes32 itemID) public constant returns(bool isIndeed) {
        return activeItems[itemID].listPointer == 0;
    }
    
    function purchase(bytes32 itemID) public payable returns(bool) {
        require(!isOpen(itemID));
        require(activeItems[itemID].price == msg.value);
        if (!activeItems[itemID].seller.send(msg.value)) {
            revert(); // if we can't pay it forward, send it back
        }
        purchased[keccak256(itemID, msg.sender)] = activeItems[itemID].hashID;
        // deleteItem(itemID);
        return true;
    }

    function newItem(uint _price, string _title, string _hashID) public returns(bool success) {
        bytes32 itemID = keccak256(_title, msg.sender);
        require(isOpen(itemID));
        activeItems[itemID].price = _price * 1 ether;
        activeItems[itemID].title = _title;
        activeItems[itemID].seller = msg.sender;
        activeItems[itemID].hashID = _hashID;
        activeItems[itemID].listPointer = itemList.push(itemID);
        purchased[keccak256(itemID, msg.sender)] = activeItems[itemID].hashID;
        return true;
    }

    function deleteItem(bytes32 itemID) private returns(bool success) {
        require(!isOpen(itemID));
        uint rowToDelete = activeItems[itemID].listPointer;
        activeItems[itemID].listPointer = 0;
        if (rowToDelete < itemList.length) {
            bytes32 keyToMove = itemList[itemList.length-1];
            itemList[rowToDelete-1] = keyToMove;
            activeItems[keyToMove].listPointer = rowToDelete;
        }
        itemList.length--;
        return true;
    }  
    
    function viewList() public constant returns (bytes32[]) {
        return itemList;
    }
    
    function viewPurchased(bytes32 itemID) public constant returns (string) {
        return purchased[keccak256(itemID, msg.sender)];
    }
    
    function getInfo(bytes32 key) public constant returns (uint, string, bytes32) {
        require(!isOpen(key));
        return (activeItems[key].price / 1 ether, activeItems[key].title, key);
    }
}