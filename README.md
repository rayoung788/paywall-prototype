# paywall-prototype

## Summary

The goal is to prototype paid file transfer between clients using IPFS for storage and the Ethereum blockchain for contract details. What works:
1. Users can upload files to IPFS with a price tag by clicking the upload button. A corresponding entry also gets added to the Ethereum contract.
2. Users can view posted files by clicking the refresh button.
3. Users can purchase files by clicking on the file name in the purchase column of the File Store table.
4. Users can download files that they own by clicking on the key in the download column of the File Store table.

## IPFS Information

``` bash
# Local install is no longer needed; a public gateway is now being used

# Gateway: 
http://ec2-18-220-209-189.us-east-2.compute.amazonaws.com:5558

# API: 
http://ec2-18-220-209-189.us-east-2.compute.amazonaws.com:5559
```

## Testrpc Setup

``` bash
# install testrpc
npm install -g ethereumjs-testrpc

# start testrpc
testrpc

# install MetaMask
https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn

# add a test account to MetaMask
http://truffleframework.com/docs/advanced/truffle-with-metamask

```

## Build Setup

``` bash
# install dependencies
npm install

# compile contracts
truffle compile

# deploy contracts
truffle migrate

# serve with hot reload at localhost:8090
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Caveats

1. Assuming files are shared among peers in IPFS, then peers would also have your files (without buying them).
2. Once you have the hashed IPFS link, there is nothing stopping you from sharing that link. I don't think IPFS has implemented file permissions (correct me if I'm wrong).
3. There is no interface on the contract to view the IPFS hash (unless you own it), but it may still be visible on the transaction block (by miners and others looking at the blockchain history).

Seems like there needs to be some extra (public-key) encryption here...
